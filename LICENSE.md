**Permission has been granted exclusively to the GShade project for distribution rights to presets in this repository, whose licenses or copyrights otherwise do not allow for redistribution, rehosting, or automated downloading. Please contact the creators directly for permission if you are interested in doing so:**

* Aeon Legion (instagram.com/aeonlegion)
  * ColorsReborn
* Alma Emma (twitter.com/AlmaEmma_ffxiv)
  * AlmaStudio
* Ares Stardust (twitter.com/neonhighways)
  * OMGEorzea
* Aztral Okami (twitter.com/AztralOkami)
  * AztralSuperBloomStudio
* Cyane Monis (twitter.com/ffxiv_nya)
  * CyanePrism
* Fairy (instagram.com/fairyqueen.xiv)
  * Fairy
* G'thro Reddia (twitter.com/RedhadesFFXIV)
  * G'thro
* Glace Camelot (twitter.com/GlaceFF14)
  * Glace
* Grey Fantasies/Lady Arkana (twitter.com/grey_fantasies)
  * Arkana
* Johnni Maestro (twitter.com/Johnnicles)
  * Johnni
* Johto (twitter.com/JohtoXIV)
  * Johto's Studio
* Kiyomi Lavellan
  * KiyomiKiyowabunga
* Kupaii Satil (twitter.com/PastelKupaii)
  * Kupaii
* Leeja Llen (leeja-ffxiv.carrd.co)
  * Leeja Llen
* Lufreine (twitter.com/lufreine)
  * Lufreine
* Malkovich Malkovich (twitter.com/malkovichff14)
  * Malkovich
* Mana (twitter.com/milkisslut)
  * Witch Presets
* Marot Satil (twitter.com/MarotSatil)
  * GShade
* Meia Mochi (twitter.com/mepururu)
  * MeiaIridescence
* Messy Fantasia (twitter.com/Mesaana10)
  * MessyPortraitTiltShift
* Mori Tori aka miiolk (twitter.com/torii_ff14)
  * Angelite
* Neneko Neko (twitter.com/xelyanne)
  * Neneko ColorS
  * Neneko ColorS - World
  * Neneko ColorS - World 2
  * Neneko ColorS - Universe
  * Neneko ColorS - World of Warcraft
  * Neneko ColorS - WoW
  * Neneko ColorS - Zero
* Nightingale Silence (twitter.com/franandneo)
  * Nightingale
* ninjafada (sfx.thelazy.net/users/u/ninjafada)
  * ninjafada
* NuclearFuzion (twitter.com/Zhvowa)
  * NukeGP
* Oni Akuma (Oni Akuma of Gilgamesh)
  * OniAkumaTrueLifeRealism
* Packetdancer (twitter.com/Packetdancer)
  * Packetdancer
* Freya "The Photosmith" Bloodmourne (twitter.com/Freybae_ffxiv)
  * PhotosmithFrey
* Ruri (twitter.com/nihil242)
  * Ruri
* Serenaya Carrin (twitch.tv/serenayacarrin)
  * SerenayaRealismGameplay
* Shenova Shadowheart (twitter.com/Shenova82)
  * Shenova
* Talim (instagram.com/talimffxiv)
  * Talim - Ta-hee!
* Tayang Mol
  * Tayang 
* Veylind Xaan (Discord: Veylind#3679)
  * CrystalCaliberII
* Yae (twitter.com/lapismenangis)
  * Nacht
* Yaz (instagram.com/alcred.ffxiv)
  * Yaz-Alcred
* Yomigami Okami (twitter.com/Yomigammy)
  * Yomigami Okami
* Yulia Rose (michanpyon.com)
  * Yulia
